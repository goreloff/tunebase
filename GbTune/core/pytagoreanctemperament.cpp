#include "pytagoreanctemperament.h"

PytagoreanCTemperament::PytagoreanCTemperament(void) : Temperament("PytagoreanCTemperament") {}

PytagoreanCTemperament::~PytagoreanCTemperament(void) {}

double PytagoreanCTemperament::GetTemperament(int note_index) const {
	return shifts.at(note_index);
}

std::vector<double> PytagoreanCTemperament::shifts(12);
void PytagoreanCTemperament::InitShifts() {
	shifts.push_back(0.00);
	shifts.push_back(13.69);
	shifts.push_back(3.91);
	shifts.push_back(-5.97);
	shifts.push_back(7.82);
	shifts.push_back(-1.96);
	shifts.push_back(11.73);
	shifts.push_back(1.96);
	shifts.push_back(15.64);
	shifts.push_back(5.87);
	shifts.push_back(-3.91);
	shifts.push_back(9.78);
}