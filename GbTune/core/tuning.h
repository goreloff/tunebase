#pragma once

#include <float.h>
#include <math.h>
#include <vector>
#include <memory>
#include "tone.h"
#include "temperament.h"

/** Provides means for tuning a note based on an equal scale from C-B (C, C#, D, D#, E, F, F#, G, G#, A, A#, B) */
class Tuning
{
private:
	/** The pitch offset in hz that is added to the reference frequency */
	double pitch_offset_;

	/** A vector of frequencies tuned with the current parameters */
	std::vector<double> frequencies_;

	/** An array of names for each note in English */
	static std::string names_[];

	/** The reference ferquency. Has to be an A */
	static double reference_frequency_;

	/** The octave of the reference frequency */
	static int reference_octave_;

	/** The current temperament for the tone calculation */
	std::shared_ptr<Temperament> temperament_;

	/** Gets the reference frequency and adds the pitch_offset 
		@see pitch_offset_
	*/ 
	double GetReferenceFrequency();

	/** Initializes the tempered frequencies based on the pitch_offset and the curren temperament 
		@see pitch_offset_
		@see temperament_
	*/
	void InitFrequencies();

	/** Calculates the given hz into the range of the reference frequency */
	double NormalizeFrequency(double hz);

	/** Calculates the closest frequency of the given hz. 
		@param hz The input frequency
		@param note_index The index of the found freqeuncy will be stored in this reference
		@param octave_shift The shift to the next or previous octave if closest hz not in standard range
	*/
	double ClosestFrequency(const double hz, int &note_index, int &octave_shift);

public:
	/** Default CTor */
	Tuning(void);

	/** Default DTor */
	~Tuning(void);

	/** Computes the difference between two frequencies in cents.
		@param base_frequency The base for the calculation
		@param compare_frequency The base frequency will be compared to this frequency
	*/
	double GetDifferenceCents(const double base_frequency, const double compare_frequency);

	/** Computes a tone that is tuned based on the currently set parameters. 
		The calculation will first find the closest note frequency in a normalized
		range. The difference will be calculated and stored in the result instance as cents
		@return The closes tone to the given frequency
	*/
	Tone GetTone(const double frequency);

	/** The pitch offset in hz that is added to the reference frequency */
	double get_pitch_offset(){
		return pitch_offset_;
	}

	/** Sets the pitch offset in hz.
		@param pitch_offset in hz
	*/
	void set_pitch_offset(double pitch_offset) {
		pitch_offset_ = pitch_offset;
		InitFrequencies();
	}

	/** The current temperament. */
	std::shared_ptr<Temperament> get_temperament(){
		return temperament_;
	}

	/**  Sets the temperament used for the tone calculation */
	void set_temperament(std::shared_ptr<Temperament> temperament){
		temperament_ = temperament;
		InitFrequencies();
	}

};