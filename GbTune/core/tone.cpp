#include "tone.h"

Tone::Tone(std::string name) {
	name_ = name;
	id_ = -1;
	frequency_ = 0.0;
	octave_ = 0;
	offset_cents_ = 0.0;
}

Tone::Tone(void)
{
	id_ = -1;
	name_ = "";
	frequency_ = 0.0;
	octave_ = 0;
	offset_cents_ = 0.0;
}


Tone::~Tone(void)
{
}
