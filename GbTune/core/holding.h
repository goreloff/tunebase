#pragma once
#include "tone.h"

/** Performs holding of detected tones. The attack time is set to 350ms */
class Holding
{

private:

	/** The min attack time in ms */
	static double min_attack_ms_;

	/** MS per sample */
	double sample_ms_;

	/** The used sample rate */
	double sample_rate_;

	/** The lenght of one sample */
	double sample_length_;

	/** How much tones are stored for attack, holding and release time, calculated on base of min_attack_time_ms_ and sample_ms_ */
	int holding_count_;

	/** The weighted average sum for cents smoothing*/
	double average_sum_;

	/** An array of tone ids for attack and release time calculations */
	int* tones_;

	/** In holding state or not */
	bool holding_;

	/** The currently holded tone */
	Tone holded_tone;

public:

	/** Constructs a holding instance
		@param sampleRate The used sample rate
		@param sampleLength The lenght of one sample (raw without zeropadding)
	*/
	Holding(double sampleRate, double sampleLength);
	~Holding(void);

	/** Processes a tone 
		@param detectedTone Pass the currently detected tone
		@param tone Will be set to holded tone when in holding phase or to the detected tone when not. Smoothing ofcents is applied
		@return True if tone detected, False otherwise
	*/
	bool process(Tone detectedTone, Tone& tone);

	/** Used to shift the underlying array without adding another tone to it by ms/sample
		@param newId The id of the new tone or -1 if non detected
	*/
	void shift( int newId );

	/** Getter for the sample rate 
		@return The set sample rate
	*/
	double get_sample_rate() {
		return sample_rate_;
	}

	/** Setter for the sample length 
		@return The set sample length
	*/
	double get_sample_length() {
		return sample_length_;
	}
};

