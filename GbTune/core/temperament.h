#pragma once

#include <string>

/** An abstract temperament model. Used as base class for different 
	kind of temperaments.
*/
class Temperament
{
private:
	/* The name of the temperament */
	std::string name_;					

public:
	/** Default CTor */
	Temperament(void);		

	/** Constructor setting the name */
	Temperament(std::string name);	

	/** Default DTor */
	~Temperament(void);					

	/** The name of the temperament */ 
	std::string get_name() const {		
		return name_;
	}

	/** Gets the offest in cents for a tone.
		@param noteIndex A index from 0-11 depicting the index on a scale from C to B
	*/
	virtual double GetTemperament(const int note_index) const = 0;	
	
};

