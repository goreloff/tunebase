#pragma once

#include <string>

/** Data class for tones. Contains relevant information of the
detected tone and the deviation from the input frequencies */
class Tone
{

private:
	/** Name of the tone in english C-C */
	std::string name_;		

	/** The index of the note in the scale */
	int id_;

	/** The detected frequency */
	double frequency_;		

	/** The octave of the detected frequency */
	int octave_;			

	/** The offset from the detected frequency to the input frequency */
	double offset_cents_;	

public:
	/** Constructs a tone with a name */
	Tone(std::string name); 

	/** Default CTor */
	Tone(void);				

	/** Default DTor */
	~Tone(void);			

	/** Name of the tone in English (C, C#, D, D#, E, F, F#, G, G#, A, A#, B) */
	std::string get_name() {
		return name_;
	}
	
	/** Sets the name of the tone. @param name The name in English (C, C#, D, D#, E, F, F#, G, G#, A, A#, B) */
	void set_name(std::string name){
		name_ = name;
	}

	/** Sets the id of the note. @param id The index of the tone on the scale */
	void set_id(int id) {
		id_ = id;
	}

	/** The index of the tone on the scale */ 
	int get_id() {
		return id_;
	}

	/** The detected frequency in hz*/
	double get_frequency() {
		return frequency_;
	}

	/** Sets the detected frequency. @param frequency The detected frequency in hz */
	void set_frequency(double frequency){
		frequency_ = frequency;
	}

	/** The offset from the detected frequency to the input frequency in cents */
	double get_offset_cents() {
		return offset_cents_;
	}

	/** Sets the offset from the detected frequency to the input frequency in cents. @param offest_cents The offset in cents */
	void set_offset_cents(double offset_cents){
		offset_cents_ = offset_cents;
	}

	/** The octave of the detected frequency */
	int get_octave(){
		return octave_;
	}

	/** Sets the octave of the detected frequency. @param octave The octave of the detected tone */
	void set_octave(int octave) {
		octave_ = octave;
	}

};

