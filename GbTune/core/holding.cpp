#include <cmath>
#include "holding.h"
#ifdef __ANDROID__
#include "android/log.h"
#endif

Holding::Holding(double sampleRate, double sampleLength)
{
	sample_rate_ = sampleRate;
	sample_length_ = sampleLength;
	sample_ms_ = 1000 / sampleRate * sampleLength;
	holding_count_ = (int) ceil(min_attack_ms_ / sample_ms_);
	tones_ = new int[holding_count_];
	holding_ = false;
	average_sum_ = 0;

	for(int i = 0; i<holding_count_;i++){
		tones_[i] = -1;
	}

#ifdef __ANDROID__
	__android_log_print(ANDROID_LOG_DEBUG, "Holding", "count:%d, sample_ms:%f", holding_count_, sample_ms_);
#endif
}

Holding::~Holding(void)
{
	delete [] tones_;
}

bool Holding::process( Tone detectedTone, Tone& tone ) {

	tone = detectedTone;

	bool detected = true;

	if(holding_) {

		bool stillHolding = false;

		for(int i = 0; i < holding_count_; i++) { // Wher are still holding when at least one sample tone is in equal to detected

#ifdef __ANDROID__
	//__android_log_print(ANDROID_LOG_DEBUG, "Holding", "tone_:%d, holding:%d", tones_[i], holded_tone.get_id());
#endif

			if(tones_[i] - holded_tone.get_id() == 0) {
				stillHolding = true;
			}
		}

		if(stillHolding) {
			if(holded_tone.get_id() != tone.get_id()) { // set last detected tone if detected another one
				tone = holded_tone;
			} else {
				average_sum_ = (0.2 * tone.get_offset_cents()) + (1.0 - 0.2) * average_sum_;
				tone.set_offset_cents(average_sum_);
			}

			detected = true;		

		} else { // leave hold
			holding_ = false;
			average_sum_ = 0;

#ifdef __ANDROID__
	__android_log_print(ANDROID_LOG_DEBUG, "Holding", "leaving hold");
#endif
		}

	} 

	// we are not holding, see if we need to
	if(!holding_){
		tone = detectedTone;
		for(int i = 0; i < holding_count_; i++) {
			if(tones_[i] - tone.get_id() != 0) {
				detected = false;
			}
		}

		if(detected) { // enter hold
#ifdef __ANDROID__
	__android_log_print(ANDROID_LOG_DEBUG, "Holding", "entering hold");
#endif
			holding_ = true;
			holded_tone = tone;
			average_sum_ = tone.get_offset_cents();
		}

	}
	
	// shift array and add new tone
	shift(detectedTone.get_id());
	

	return detected;
}

void Holding::shift( int newid ) {
	if(holding_count_ <= 1) {
		return;
	}

	int i = 0;
	while(i < holding_count_ - 1) {
		tones_[i] = tones_[i + 1];
		i++;
	}

	tones_[holding_count_ - 1] = newid;
}

double Holding::min_attack_ms_ = 350; 