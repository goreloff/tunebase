#pragma once

#include "temperament.h"
#include <vector>

/** Implementation of the Pytagorian C Temperamanet */
class PytagoreanCTemperament : public virtual Temperament
{
public:
	/** Default CTor */
	PytagoreanCTemperament(void);

	/** Default DTor */
	~PytagoreanCTemperament(void);

	/** Stores the shifts in cents of the complete scale */
	static std::vector<double> shifts;

	/** Initializes the shifts in cents */
	static void InitShifts();

	double GetTemperament(const int note_index) const;
};

