#pragma once

#include "fft/filter/filt.h"
#include "fft/qifft.h"
#include "tone.h"
#include "tuning.h"
#include "holding.h"

/** Core Engine for predicting Tones on base of samples */
class ToneEngine
{
private:

	/** Sets a filter to be applied before the fft */
	std::shared_ptr<Filter> filter_;

	/** Controls attack and holding of detected tones */
	std::shared_ptr<Holding> holding_;

	/** Tuning instance for tone prediciton */
	Tuning tuning_;

	/** Used to perform fft */
	std::shared_ptr<QIFFT> fft_;

	/** Deprecated */
	double sensitivity_;
	double PreProcessSample(const double input, const int position, const int sampleLength);
	bool FrequencyInRange(const double frequence);
	void EnsureFFT( int windowSize, int sampleRate );
	void EnsureHolding( int sampleLenght, int sampleRate );

public:
	ToneEngine(void);
	~ToneEngine(void);

	/** Performs FFT and gets the Tone that will fit the extracted frequence. The input is an array of
		signed bytes. Method will take two bytes as one sample
		@param sample An array of signed bytes
		@param sampleLenght The lenght of the input data
		@param sampleRate The rate used for sampling
		@param tone Output parameter for the detected tone
		@return True - if a tone has been detected, False - Otherwise
	*/
	bool GetToneFor16bitRaw(signed char* sample, int sampleLength, int sampleRate, Tone& tone);

	/** Sets a filter to apply before performing the FFT
		@param The filter to apply
		@see Filter
	*/
	void set_filter(std::shared_ptr<Filter> filter){
		filter_ = filter;
		
	}

	/** Gets the current tuning for tone recognition */
	Tuning& get_tuning(){
		return tuning_;
	}

	/** Deprecated: Sets the sensivity of the prediciton.
		@param sensitivity Threshold amplitude value, default set to 0	
	*/
	void set_sensitivity(double sensitivity){
		sensitivity_ = sensitivity;
		if(fft_) {
			fft_->set_threshold(sensitivity_);
		}
	}

};

