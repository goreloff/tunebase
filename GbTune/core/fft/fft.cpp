#include "fft.h"

FFT::FFT(FFTDIRECT direction, int numSamples) {
	direction_ = direction;

	switch(direction_) {
	case FORWARD:
		config_ = kiss_fftr_alloc(numSamples, 0, NULL, NULL);
		spectrum_ = (kiss_fft_cpx*) malloc(sizeof(kiss_fft_cpx) * numSamples);
		real_signal_ = NULL;
		num_samples_ = numSamples;
		usescale_ = USESCALE_NO;
		scale_ = 1;
		break;
	case INVERSE:
		config_ = kiss_fftr_alloc(numSamples, 1, NULL, NULL);
		spectrum_ = NULL;
		real_signal_ = (kiss_fft_scalar*) malloc(sizeof(kiss_fft_scalar) * numSamples);
		num_samples_ = numSamples;
		usescale_ = USESCALE_YES;
		scale_ = 1.0f/numSamples;
		break;
	}

	
}

FFT::~FFT(void)
{
	  switch(direction_)
    {
        case FORWARD:
            free(config_);
	        free(spectrum_);
            break;
        case INVERSE:
            free(config_);
	        free(real_signal_);
            break;
    }
}

REAL FFT::CpxAbs(kiss_fft_cpx val)
{
    return (REAL)sqrtf(val.r*val.r + val.i*val.i);
}

void FFT::scale()
{
    int i;
    if( usescale_ == USESCALE_YES )
    {
        switch( direction_ )
        {
        case FORWARD:
            for( i = 0; i< num_samples_; i++ )
            {
                spectrum_[i].i *= scale_;
                spectrum_[i].r *= scale_;
            }
            break;
        case INVERSE:
            for( i = 0; i< num_samples_; i++ )
            {
                real_signal_[i] *= scale_;
            }
            break;
        }
    }       
}

void FFT::FFT_Transform()
{
    switch( direction_ )
    {
        case FORWARD:
            kiss_fftr(config_, real_signal_, spectrum_);
            scale();
            break;
        case INVERSE:
            kiss_fftri(config_, spectrum_, real_signal_);
            scale();
            break;
    }
}
