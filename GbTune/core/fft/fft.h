#pragma once

#include "kissfft/kiss_fft.h"
#include "kissfft/kiss_fftr.h"
#include "fftutils.h"

#define USESCALE_YES 'y'
#define USESCALE_NO 'n'

typedef enum fft_direction {
	FORWARD,
	INVERSE
} FFTDIRECT;

class FFT
{

protected:
	FFTDIRECT direction_;
	kiss_fftr_cfg config_;
	kiss_fft_scalar* real_signal_;
	kiss_fft_cpx* spectrum_;
	int num_samples_;
	char usescale_;
	float scale_;

private:
	void scale();

public:

	FFT(FFTDIRECT direction, int numSamples);
	~FFT(void);

	REAL CpxAbs(kiss_fft_cpx val);
	void FFT_Transform();
};