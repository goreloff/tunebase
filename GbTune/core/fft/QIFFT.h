#pragma once

#include "fft.h"

typedef enum {
    Rectangular,
    Gauss,
    Hann
} T_WINDOW;

class QIFFT : public FFT
{
private:
	
	int nfft_;
    T_WINDOW window_type_;
    REAL* window_;
    REAL* real_spectrum_;
    REAL threshold_;
    REAL* hps_;
    int sample_rate_;
	int pad_factor_;

	/** Performes HPS algorithm and corrects any octave shifts caused by harmonic detection. Returns the bins with the highest magnitude
		@param size The size of the fft samples
		@param minIndex Min index to search for harmonics
		@param maxIndex Max index to search for harmonics
		@param harmonics Number of harmonics
		@param spectrum FFT spectrum
		@return The most significant bin
	*/
	int GetMostSignificantBin(const int size, const int minIndex, const int maxIndex, const int harmonics, double* spectrum);

	/** Gets the most significant bins using HPS and based on the fft magnitudes. Allows a min threshold of 50 cent between frequencies
		@param size The size of the fft samples
		@param minIndex Min index to search for harmonics
		@param maxIndex Max index to search for harmonics
		@param harmonics Number of harmonics
		@param spectrum FFT spectrum
		@param topCount Amount of most significant bins
		@param top Array of indices for most significant bins
	*/
	void GetMostSignificantBins(const int size, const int minIndex, const int maxIndex, const int harmonics, double* spectrum, const int topCount, int top[]);

	/** Gets the most significant bin from an array of candidates. Sums magnitudes from upper and lower octaves to select the most significant bin
		@param size The size of the fft samples
		@param spectrum FFT spectrum
		@param magnitudeCount The amount of bin candidates
		@param maxMagnitudes Array of indices for bins with the max magnitude
	*/
	int GetMostSignificantFromBins(const int size, double* spectrum, const int magnitudeCount, const int maxMagnitudes[]);
	double GetDifferenceCents(const double frequency1, const double frequency2);

	int GetMaxBin( );


public:

	/** Default CTor */
	QIFFT( void );

	/** Constructor
		@param nfft Size of the sample
		@param The type of the window
		@param The threshold magnitude for frequency detection
		@param The sample rate
	*/
	QIFFT( int nfft, T_WINDOW windowType, REAL threshold, int sampleRate, int padFactor );

	/** Default DTor */
	~QIFFT( void );

	/** Computes the most significant frequency of the imput signal.
		@param The input signal
	*/
	REAL GetMostSignificantFrequency( REAL* input );
	
	/** Returns the sample lenght */
	int get_sample_lenght() {
		return nfft_;
	}

	/** Sets the sample lenght */
	void set_sample_lenght(int nfft) {
		nfft_ = nfft;
	}

	/** Gets the threshold magnitude for frequency detection*/
	REAL get_threshold() {
		return threshold_;
	}

	/** Sets the threshold magnitude for frequency detection */
	void set_threshold(REAL threshold) {
		//threshold_ = threshold;
	}

	/** Gets the sample rate */
	int get_sample_rate() {
		return sample_rate_;
	}

	/** Sets the sample rate */
	void set_sample_rate(int sample_rate) {
		sample_rate_ = sample_rate;
	}

};

