#include <stdlib.h>
#include <cmath>
#include "qifft.h"
#ifdef __ANDROID__
#include "android/log.h"
#endif

QIFFT::QIFFT( int nfft, T_WINDOW windowType, REAL threshold, int sampleRate, int padFactor ) : FFT(FORWARD, nfft)
{
	nfft_ = nfft;
	real_signal_ = (kiss_fft_scalar*)calloc(nfft,sizeof(kiss_fft_scalar));
	real_spectrum_ = (REAL*)calloc(((nfft/2)+1),sizeof(REAL));
	hps_ = (REAL*)calloc(((nfft/2)+1),sizeof(REAL));
	window_type_ = windowType;

	switch(windowType)
	{
	case Gauss:
		window_ = GetGaussWindow(nfft);
		break;
	case Hann:
		window_ = GetHannWindow(nfft);
		break;
	default:
		window_ = NULL;
		break;
	}

	threshold_ = 0;
	sample_rate_ = sampleRate;
	pad_factor_ = padFactor;

}


QIFFT::~QIFFT(void)
{
	if( real_spectrum_ != NULL )
	{
		free(real_spectrum_);
	}

	if( hps_ != NULL )
	{
		free(hps_);
	}

	if( window_ != NULL )
	{
		free(window_);
	}

}


int QIFFT::GetMaxBin( )
{
	int i;
	int max = 1;

	for( i = 2; i<=nfft_/2; i++ )
	{
		if( real_spectrum_[i] > real_spectrum_[max] )
		{
			max = i;
		}
	}

	if( real_spectrum_[max] >= threshold_ )
	{
		return max;
	}

	return 0;
}

//------------------------------------------------------------------------------
// Computes the zero crossing rate estimator of a given input buffer. This
// input can be pre-processed with a lowpass filter to increase robustness to
// noise.
//------------------------------------------------------------------------------
REAL QIFFT::GetMostSignificantFrequency( SIGNAL* input )
{
	int i;
	int bin;
	REAL i_bin = 0;
	REAL i_val = 0;
	REAL sqrtnfft = sqrt( (REAL) nfft_ );

	// Apply window function to input and copy to FFT input
	if( window_type_ == Rectangular )
	{
		for( i=0; i<nfft_ / pad_factor_; i++ )
		{
			real_signal_[i] = (kiss_fft_scalar) input[i];
		}
	}
	else
	{
		for( i=0; i<nfft_ / pad_factor_; i++ )
		{
			real_signal_[i] = input[i] * window_[i];
		}
	}


	// Transform the signal
	FFT_Transform();

	// Compute the real spectrum
	for( i=0; i<nfft_/2 + 1; i++ )
	{
		real_spectrum_[i] = CpxAbs(spectrum_[i]) / sqrtnfft;
		hps_[i] =  real_spectrum_[i];
	}

	// Identify the most significant bins
	int tops[6];
	GetMostSignificantBins(nfft_/2, 1, nfft_/2, 3, hps_, 6 , tops);
	bin = GetMostSignificantFromBins(nfft_, hps_, 6, tops);
	//bin = GetMostSignificantBin(nfft_/2, 1, nfft_/2, 3, hps_);

	if( bin == 0 || bin == 1)
	{
		// No frequency detected
		return 0;
	}
	else if( bin > nfft_/2 )
	{
		i_bin = (REAL) bin;
	}
	else
	{
		interpolate( (REAL)bin,
			logf(real_spectrum_[bin-1] + 1),
			logf(real_spectrum_[bin] + 1),
			logf(real_spectrum_[bin+1] + 1),
			&i_bin,
			&i_val );
	}

	if(real_spectrum_[bin] < threshold_) {
		return 0;
	}

	if( i_bin > 1 && i_bin <= nfft_ / 2 )
	{
		return ( (REAL) sample_rate_ / (REAL) nfft_ ) * i_bin;
	}
	
	return 0;
}

void QIFFT::GetMostSignificantBins(const int size, const int minIndex, const int maxIndex, const int harmonics, double* spectrum, const int topCount, int top[]){
	
	for (int i = 0; i < topCount; i++) {
		top[i] = i;
	}

	// set the maximum index to search for a pitch
	int i, j, maxHIndex;
	maxHIndex = size/harmonics;
	if (maxIndex < maxHIndex) {
		maxHIndex = maxIndex;
	}

	// generate the Harmonic Product Spectrum values
	int maxLocation = minIndex;
	int second = minIndex;

	for (j=minIndex; j<=maxHIndex; j++) {
		for (i=1; i<=harmonics; i++) {  // i=1: double the fundamental
			spectrum[j] *= spectrum[j*i];
		}
	}

	// calculate the top magnitudes
	for (int i = topCount; i < size / 2; i++)
	{
		// find out the smallest 'max so far' number
		int m = 0;
		for (int j = 0; j < topCount; j++){
			if (spectrum[top[j]] < spectrum[top[m]]){
				m = j;
			}
		}

		// if our current number is bigger than the smallest stored, replace it
		if (spectrum[i] > spectrum[top[m]] && std::fabs(GetDifferenceCents(i * sample_rate_ / nfft_, m * sample_rate_ / nfft_)) > 50.0){
			top[m] = i;
		}
	}

}

int QIFFT::GetMostSignificantFromBins(const int size, double* spectrum, const int magnitudeCount, const int maxMagnitudes[]) {

	const double binFrequency = ((REAL) sample_rate_ / (REAL) nfft_ );
	const double binSmaples = ( (REAL) nfft_ / (REAL) sample_rate_ );
	const double octaveUpCount = 2;

	// TODO: avoid allocation
	double * magnitudes = new double[magnitudeCount];

	for(int i = 0; i < magnitudeCount; i++) {
		double frequency = maxMagnitudes[i] * binFrequency;
		magnitudes[i] = 0;

		// sum up octaves
		for(int j = 0; j<octaveUpCount;j++){
			int octaveBin = (frequency * pow(2,j)) * binSmaples;
			if(octaveBin <= size / 2) {
				magnitudes[i] += spectrum[octaveBin];
			}
		}

		// add one octave down
		int octaveBin = (frequency * pow(2,-1)) * binSmaples;
		if(octaveBin >= 0) {
			magnitudes[i] += spectrum[octaveBin];
		}
			

#ifdef __ANDROID__
	//__android_log_print(ANDROID_LOG_DEBUG, "FFT", "freq:%f, mag:%f", frequency, magnitudes[i]);
#endif

	}

	// get highest magnitude
	double max = 0;
	int maxIndex = 0;
	for(int i = 0; i < magnitudeCount; i++){
		if(magnitudes[i] > max){
			max = magnitudes[i];
			maxIndex = i;
		}
	}

#ifdef __ANDROID__
	//__android_log_print(ANDROID_LOG_DEBUG, "FFT", "=> max:%d ", maxIndex);
#endif

	delete [] magnitudes;
	return maxMagnitudes[maxIndex];

	}


int QIFFT::GetMostSignificantBin(const int size, const int minIndex, const int maxIndex, const int harmonics, double* spectrum) {

	// set the maximum index to search for a pitch
	int i, j, maxHIndex;
	maxHIndex = size/harmonics;
	if (maxIndex < maxHIndex) {
		maxHIndex = maxIndex;
	}

	// generate the Harmonic Product Spectrum values and keep track of the
	// maximum amplitude value to assign to a pitch.
	int maxLocation = minIndex;

	for (j=minIndex; j<=maxHIndex; j++) {
		for (i=1; i<=harmonics; i++) {  // i=1: double the fundamental
			spectrum[j] *= spectrum[j*i];
		}
		if (spectrum[j] > spectrum[maxLocation]) {
			maxLocation = j;
		} 
	}

	// Correct for octave too high errors.  If the maximum subharmonic
	// of the measured maximum is approximately 1/2 of the maximum
	// measured frequency, AND if the ratio of the sub-harmonic
	// to the total maximum is greater than 0.2, THEN the pitch value
	// is assigned to the subharmonic.

	int max2 = minIndex;
	int maxsearch = maxLocation * 3 / 4;
	for (i=minIndex+1; i<maxsearch; i++) {
		if (spectrum[i] > spectrum[max2]) {
			max2 = i;
		}
	}

	if (abs(max2 * 2 - maxLocation) < 4) {
		if (spectrum[max2]/spectrum[maxLocation] > 0.2) {
			maxLocation = max2;
		}
	}

	return maxLocation;
}

double QIFFT::GetDifferenceCents(const double frequency1, const double frequency2){
	return 1200 *  log(frequency2 / frequency1) / log(2);
}
