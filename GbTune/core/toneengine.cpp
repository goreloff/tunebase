#include <cmath>
#include <exception>
#include <iostream>
#include "toneengine.h"
#ifdef __ANDROID__
#include "android/log.h"
#endif

ToneEngine::ToneEngine(void)
{
	sensitivity_ = 0;
}


ToneEngine::~ToneEngine(void)
{

}

bool ToneEngine::GetToneFor16bitRaw(signed char* sample, int sampleLength, int sampleRate, Tone& tone) {
	const int padFactor = 2;

	if(filter_) {
		filter_->init();
	}

	int lenght = sampleLength / 2;
	int windowSize = (lenght * padFactor);

	double* padSample = new double[windowSize];

	for (int i = 0; i < sampleLength; i += 2) {
		padSample[i >> 1] = (short) ((sample[i] & 0xFF) | ((sample[i + 1] & 0xFF) << 8));
		padSample[i >> 1] = (float) PreProcessSample(padSample[i >> 1], i >> 1, sampleLength / 2);
	}

	zeroPad(padSample, windowSize, lenght);
	EnsureHolding(lenght, sampleRate);
	EnsureFFT(windowSize, sampleRate);
	double frequency = fft_->GetMostSignificantFrequency(padSample);	

	delete [] padSample;
	padSample = NULL;

	// Discard all samples with freq == 0
	if(!FrequencyInRange(frequency)) {
		// shift samples in any case
		holding_->shift( -1 );
		return false;
	}

	// Get the closest tone
	Tone detectedTone = tuning_.GetTone(frequency);
	// Get holding
	bool detected = holding_->process(detectedTone, tone);
	return detected;

}

void ToneEngine::EnsureHolding( int sampleLenght, int sampleRate ) {

	if(!holding_) {
		std::shared_ptr<Holding> holding (new Holding( sampleRate, sampleLenght ));
		holding_ = holding;
	} else if (holding_->get_sample_length() != sampleLenght) {
		holding_.reset(new Holding( sampleRate, sampleLenght ));
	} else if(holding_->get_sample_rate() != sampleRate) {
		holding_.reset(new Holding( sampleRate, sampleLenght ));
	}
}

void ToneEngine::EnsureFFT( int windowSize, int sampleRate ) {
	try
	{
		if(!fft_){
			std::shared_ptr<QIFFT> fft (new QIFFT(windowSize, Gauss, sensitivity_ , sampleRate, 2));
			fft_ = fft;
		} else if (fft_->get_sample_lenght() != windowSize) {
			fft_.reset(new QIFFT(windowSize, Gauss, sensitivity_ , sampleRate, 2));
		} else if (fft_->get_sample_rate() != sampleRate) {
			fft_->set_sample_rate(sampleRate);
		}

	}catch (std::exception& e) {
		std::cout << "Standard exception: " << e.what() << std::endl;
	}
}

double ToneEngine::PreProcessSample(const double input, const int position, const int sampleLenght){
	double sample = input;

	if(filter_) {
		sample = filter_->do_sample(input);
	}

	return sample;
}

bool ToneEngine::FrequencyInRange(const double frequency) {

	if(frequency <= 0) {
		return false;
	}

	if(!filter_) {
		return true;
	}

	switch (filter_->get_type())
	{

	case BPF:
		return frequency > filter_->get_fl() && frequency < filter_->get_fu();
	case LPF:
		return frequency < filter_->get_fx();
	case HPF:
		return frequency > filter_->get_fx();
	default:
		break;
	}

	return true;
}
