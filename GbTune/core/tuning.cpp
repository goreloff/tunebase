#include <cmath>
#include <iostream>   
#include "tuning.h"
#include "equivalenttemperament.h"

Tuning::Tuning(void)
{
	pitch_offset_ = 0.0;
	
	std::shared_ptr<Temperament> temperament(new EquivalentTemperament());
	temperament_ = temperament;
	InitFrequencies();
}

Tuning::~Tuning(void)
{
	
}

Tone Tuning::GetTone(const double frequency){

	// get into the standard range to make things easier
	double normalizedfrequency = NormalizeFrequency(frequency);
	
	// closest candidate
	int noteIndex = 0;
	int octaveShift = 0;
	double closestFrequency = ClosestFrequency(normalizedfrequency, noteIndex, octaveShift);

	// reference_frequency_ is in octave reference_octave_
	int octave = reference_octave_ + (int) (GetDifferenceCents(normalizedfrequency, frequency) / 1200) + octaveShift;
	double octaveFrequency =  closestFrequency * pow(2,  octave - reference_octave_ - octaveShift);

	Tone tone = Tone(names_[noteIndex]);
	tone.set_id(noteIndex);
	tone.set_octave(octave);
	tone.set_frequency(octaveFrequency);
	tone.set_offset_cents(GetDifferenceCents(octaveFrequency, frequency));

	return tone;

}

double Tuning::GetDifferenceCents(const double frequency1, const double frequency2){
	return 1200 *  log(frequency2 / frequency1) / log(2);
}

double Tuning::NormalizeFrequency(double hz) {
    if (hz == 0.0) return 0.0;
    
	while (hz < frequencies_[0]) {
		hz = 2 * hz;
	}
	while (hz > frequencies_[11]) {
		hz = 0.5 * hz;
	}
	return hz;
}

double Tuning::ClosestFrequency(const double hz, int &noteindex, int &octaveshift) {

	double minDist = DBL_MAX;
	double minFreq = -1;
	for (int i = 0; i < 12; i++) {
		double dist = std::abs(frequencies_[i] - hz);
		if (dist < minDist) {
			minDist = dist;
			minFreq = frequencies_[i];
			noteindex = i;
		}
	}

	// check for next lower octave
	double dist = std::abs(frequencies_[11] / 2 - hz);
	if(dist < minDist) {
		minDist = dist;
		minFreq = frequencies_[11] / 2;
		noteindex = 11;
		octaveshift--;
	}

	// check for next higher octave
	dist = std::abs(frequencies_[0] * 2 - hz);
	if(dist < minDist) {
		minDist = dist;
		minFreq = frequencies_[0] * 2;
		noteindex = 0;
		octaveshift++;
	}

	return minFreq;

}

void Tuning::InitFrequencies(){
	frequencies_.reserve(12);

	if(!frequencies_.empty()) {
		frequencies_.clear();
	}

	// We want one octave that contains the reference frequency (A) starting with C (-900 cents to A), going to B (+100 cents to A)
	int noteIndex = 0;
	double referenceFrequency = GetReferenceFrequency();

	for(int cents = -900; cents <= 200; cents +=100) {
		double temperedCents = cents + temperament_->GetTemperament(noteIndex++);
		frequencies_.push_back(referenceFrequency * std::pow(2, temperedCents / 1200.0));
	}
}

double Tuning::GetReferenceFrequency(){
	return reference_frequency_ + pitch_offset_;
}

std::string Tuning::names_[] =  { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
double Tuning::reference_frequency_ = 440.0;
int Tuning::reference_octave_ = 4;