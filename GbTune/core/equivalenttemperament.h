#pragma once

#include "temperament.h"

/** Default Temperation that is euqally distributed */
class EquivalentTemperament : public virtual Temperament
{

public:
	/** Default CTor */
	EquivalentTemperament(void);

	/** Default DTor */
	~EquivalentTemperament(void);

	double GetTemperament(const int noteIndex) const;

};

