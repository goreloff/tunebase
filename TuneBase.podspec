Pod::Spec.new do |spec|
  spec.name          = "TuneBase"
  spec.version       = "0.1.0"
  spec.license       = "MIT"
  spec.homepage      = "https://github.com/<GITHUB_USERNAME>/TuneBase"
  spec.authors       = { "Wolfgang Damm" => "wolfgang@pocket-lifestyle.com" }
  spec.summary       = "tune base library"
  spec.source        = { :git => "https://goreloff@bitbucket.org/goreloff/tunebase.git", :tag => "0.1.0" }

  spec.platform     = :ios, "11.0"
  
  spec.requires_arc = true
  spec.libraries = "stdc++"
  
  spec.source_files       = 'Pod/Classes/*.{h,mm}', 'GbTune/core/**/*.{h,c,cpp}'
  spec.public_header_files  = 'Pod/Classes/PLTTuneEngine.h'
  spec.private_header_files  = 'GbTune/core/**/*.h'
  #spec.header_mappings_dir = 'GbTune/core'
end