
#import "PLTTuneEngine.h"
#import "tuning.h"
#import "tone.h"
#import "toneengine.h"
#import "tuning.h"

@interface PLTTuneEngine ()
@property (nonatomic) ToneEngine* engine;
@property (nonatomic) Tone tone;
@end

@implementation PLTTuneEngine

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc {
    delete _engine;
}

- (void)start {
    _engine = new ToneEngine();
    _engine->set_sensitivity(1.0);
    _engine->get_tuning().set_pitch_offset(self.standardPitch - 440);
}

- (void)stop {
    delete _engine;
    _engine = nil;
}

- (ToneInfo) toneForSample: (NSData*) sample {
    ToneInfo result;
    
    double sum = 0.0;
    int16_t* ptr = (int16_t*)sample.bytes;
    int16_t* end = ptr + [sample length] / 2;
    while (ptr < end) {
        sum += abs(*ptr);
        ptr++;
    }
    
    NSMutableData* samplePlusPadding = [sample mutableCopy];
    [samplePlusPadding setLength: samplePlusPadding.length * 2];
    int16_t* sampleBytes = (int16_t*)[samplePlusPadding mutableBytes];
    
    result.intensity = sum / ([sample length] / 8);

    if (sampleBytes && _engine && _engine->GetToneFor16bitRaw((signed char*)sampleBytes, [sample length] / 2, 44100, _tone)) {
        result.frequency = _tone.get_frequency();
        result.toneId = _tone.get_id();
        result.octave = _tone.get_octave();
        result.centsOff = _tone.get_offset_cents();
    } else {
        result.toneId = -1;
        result.centsOff = 0;
        result.frequency = 0;
    }
    
    return result;
}

- (void)setStandardPitch:(NSInteger)standardPitch {
    _standardPitch = standardPitch;
    
    if (_engine) {
        _engine->get_tuning().set_pitch_offset(standardPitch - 440);
    }
}

@end