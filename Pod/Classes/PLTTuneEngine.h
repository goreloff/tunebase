//
//  TuneEngine.h
//  Pods
//
//  Created by Wolfgang Damm on 17/09/15.
//
//

#ifndef TuneEngine_h
#define TuneEngine_h

//    /** The index of the note in the scale */
//    int id_;
//    
//    /** The detected frequency */
//    double frequency_;
//    
//    /** The octave of the detected frequency */
//    int octave_;
//    
//    /** The offset from the detected frequency to the input frequency */
//    double offset_cents_;
//};

typedef struct {
    NSInteger toneId;
    double intensity;
    double frequency;
    double centsOff;
    NSUInteger octave;
} ToneInfo;

@interface PLTTuneEngine : NSObject

@property (nonatomic) NSInteger standardPitch;

- (void) start;
- (void) stop;
- (ToneInfo) toneForSample: (NSData*) sample;

@end

#endif /* TuneEngine_h */
